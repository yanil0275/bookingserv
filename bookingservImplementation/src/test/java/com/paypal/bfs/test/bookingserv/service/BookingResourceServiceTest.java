package com.paypal.bfs.test.bookingserv.service;

import com.paypal.bfs.test.bookingserv.api.model.Address;
import com.paypal.bfs.test.bookingserv.api.model.Booking;
import com.paypal.bfs.test.bookingserv.exception.InValidRequestParameterException;
import com.paypal.bfs.test.bookingserv.repository.BookingRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

public class BookingResourceServiceTest {

    @InjectMocks
    BookingResourceService bookingResourceService;

    @Mock
    BookingRepository bookingRepository;

    List<Booking> listBookings = new ArrayList<>();
    Booking mockBooking = new Booking();

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        mockBooking.setId(1);
        mockBooking.setFirstName("anil");
        mockBooking.setLastName("yadav");
        mockBooking.setDateOfBirth(new Date());
        mockBooking.setCheckinDatetime(new Date());
        mockBooking.setCheckoutDatetime(new Date());
        mockBooking.setTotalPrice(2345.4);

        Address address = new Address();
        address.setId(1);
        address.setLine1("india");
        address.setState("rajasthan");
        address.setCity("Sikar");
        address.setZipcode("332222");
        mockBooking.setAddress(address);
        listBookings.add(mockBooking);
    }

    @Test
    public void testFindAllBookings()
    {

        Mockito.when(bookingRepository.findAll()).thenReturn(listBookings);
        List<Booking> bList = bookingResourceService.getBookings();
        assertEquals(1, bList.size());
        verify(bookingRepository, times(1)).findAll();
    }

    @Test(expected = InValidRequestParameterException.class)
    public void testCreateBooking()
    {
        bookingResourceService.createBooking(mockBooking);
    }

    @Test
    public void testValidateFields()
    {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 15);
        mockBooking.setCheckinDatetime(c.getTime());
        mockBooking.setCheckoutDatetime(c.getTime());
        c.add(Calendar.YEAR, -19);
        mockBooking.setDateOfBirth(c.getTime());
        bookingResourceService.validateFields(mockBooking);
    }
}
