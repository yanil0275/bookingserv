package com.paypal.bfs.test.bookingserv.impl;

import com.paypal.bfs.test.bookingserv.api.model.Address;
import com.paypal.bfs.test.bookingserv.api.model.Booking;
import com.paypal.bfs.test.bookingserv.service.BookingResourceService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = BookingResourceImpl.class)
public class BookingResourceImplTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookingResourceService bookingResourceService;

    String demoBooking = "{\n" +
            "    \"first_name\":\"anil\",\n" +
            "    \"last_name\":\"yadav\",\n" +
            "    \"date_of_birth\" : \"1996-07-08\",\n" +
            "    \"checkin_datetime\" : \"2023-21-01T09:45:00.000+02:00\",\n" +
            "    \"checkout_datetime\" : \"2023-22-01T09:45:00.000+02:00\",\n" +
            "    \"totalPrice\": \"2345.6\",\n" +
            "    \"Address\":{\n" +
            "        \"line1\":\"india\",\n" +
            "        \"line2\":\"india\",\n" +
            "        \"city\":\"sikar\",\n" +
            "        \"state\":\"rajasthan\",\n" +
            "        \"zipcode\":332023\n" +
            "    }\n" +
            "\n" +
            "}";

    List<Booking> listBookings = new ArrayList<>();
    Booking mockBooking = new Booking();

    @Before
    public void setUp() {
        mockBooking.setId(1);
        mockBooking.setFirstName("anil");
        mockBooking.setLastName("yadav");
        mockBooking.setDateOfBirth(new Date());
        mockBooking.setCheckinDatetime(new Date());
        mockBooking.setCheckoutDatetime(new Date());
        mockBooking.setTotalPrice(2345.4);

        Address address = new Address();
        address.setId(1);
        address.setLine1("india");
        address.setState("rajasthan");
        address.setCity("Sikar");
        address.setZipcode("332222");
        mockBooking.setAddress(address);
        listBookings.add(mockBooking);
    }


    @Test
    public void createBooking() throws Exception {

        Mockito.when(bookingResourceService.createBooking(Mockito.any(Booking.class))).thenReturn(mockBooking);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/v1/bfs/booking")
                .accept(MediaType.APPLICATION_JSON).content(demoBooking)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());

    }

    @Test
    public void testGetAllBooking() throws Exception {

        Mockito.when(bookingResourceService.getBookings()).thenReturn(listBookings);
        mockMvc.perform(get("/v1/bfs/allBookings"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].first_name", Matchers.is("anil")));
    }
}
