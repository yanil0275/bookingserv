package com.paypal.bfs.test.bookingserv.impl;

import com.paypal.bfs.test.bookingserv.api.BookingResource;
import com.paypal.bfs.test.bookingserv.api.model.Booking;
import com.paypal.bfs.test.bookingserv.exception.InValidRequestParameterException;
import com.paypal.bfs.test.bookingserv.service.BookingResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BookingResourceImpl implements BookingResource {

    BookingResourceService bookingResourceService;
    @Autowired
    public BookingResourceImpl(BookingResourceService bookingResourceService) {
        this.bookingResourceService = bookingResourceService;
    }


    @Override
    public ResponseEntity<Booking> create(Booking booking) throws InValidRequestParameterException {
        Booking _booking = bookingResourceService.createBooking(booking);
        return new ResponseEntity<>(_booking, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<Booking>> getAllBookings() {
        List<Booking> bookingList = bookingResourceService.getBookings();
        if (bookingList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(bookingList, HttpStatus.OK);
    }

}
