package com.paypal.bfs.test.bookingserv.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InValidRequestParameterException extends RuntimeException {
    public InValidRequestParameterException(String exception) {
        super(exception);
    }
}
