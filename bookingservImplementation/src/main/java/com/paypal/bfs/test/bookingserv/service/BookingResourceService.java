package com.paypal.bfs.test.bookingserv.service;

import com.paypal.bfs.test.bookingserv.exception.InValidRequestParameterException;
import com.paypal.bfs.test.bookingserv.repository.BookingRepository;
import com.paypal.bfs.test.bookingserv.api.model.Booking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
public class BookingResourceService {

    BookingRepository bookingRepository;

    @Autowired
    BookingResourceService(BookingRepository bookingRepository){
        this.bookingRepository =bookingRepository;
    }

    @Transactional
    public Booking createBooking(Booking booking) {
        validateFields(booking);
        return bookingRepository.save(booking);
    }

    public List<Booking> getBookings() {
        return bookingRepository.findAll();
    }

    public void validateFields(Booking booking){
        Date checkinDate = booking.getCheckinDatetime();
        Date checkoutDate = booking.getCheckoutDatetime();
        Date dob = booking.getDateOfBirth();
        Date currentDate = new Date();
        Double amount = booking.getTotalPrice();
        String firstname = booking.getFirstName();
        String lastname = booking.getLastName();
        String line1 = booking.getAddress().getLine1();
        String city = booking.getAddress().getCity();
        String state = booking.getAddress().getState();
        String zipcode = booking.getAddress().getZipcode();


        if(checkinDate ==null){
            throw new InValidRequestParameterException("checkin_datetime must not be empty");
        }
        else if(!checkinDate.after(currentDate)){
            throw new InValidRequestParameterException("checkin_datetime must be greater than the current date");
        }
        if(dob ==null){
            throw new InValidRequestParameterException("date_of_birth must not be empty");
        }
        else if(dob.after(currentDate)){
            throw new InValidRequestParameterException("date_of_birth must not be greater than the current date");
        }
        if(checkoutDate ==null){
            throw new InValidRequestParameterException("checkoutDate must not be empty");
        }
        else if(!checkinDate.after(currentDate)){
            throw new InValidRequestParameterException("checkoutDate must be greater than the current date");
        }
        if(amount == null || !(amount > 0)){
            throw new InValidRequestParameterException("totalPrice must not be empty or zero");
        }
        if(firstname == null || firstname.isEmpty()){
            throw new InValidRequestParameterException("first_name must not be empty");
        }
        if(lastname == null || lastname.isEmpty()){
            throw new InValidRequestParameterException("last_name must not be empty");
        }
        if(line1 == null || line1.isEmpty()){
            throw new InValidRequestParameterException("line1 must not be empty");
        }
        if(city == null || city.isEmpty()){
            throw new InValidRequestParameterException("city must not be empty");
        }
        if(state == null || state.isEmpty()){
            throw new InValidRequestParameterException("state must not be empty");
        }
        if(zipcode == null || zipcode.isEmpty()){
            throw new InValidRequestParameterException("zipcode must not be empty");
        }
        else if(!zipcode.matches( "\\d{6}" )){
            throw new InValidRequestParameterException("zipcode is not valid");
        }
    }
}
